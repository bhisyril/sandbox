@extends('master')
@section('content')
<h1>Post Page</h1>
<form action="" method="POST">
    {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-md-6">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" required name="title">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="content">Content</label>
                <textarea class="form-control" id="content" rows="3" name="content"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
            </div>        
        </div>
</form>
@endsection